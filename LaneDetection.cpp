/*
Written by Nathan Brinda
last modified: 7/13/2017
*/

#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>
#include <time.h>
using namespace std;
using namespace cv;

Mat processImage(Mat img);
Mat ROI(Mat img, vector<vector<Point>> vertices);
void draw(Mat original_img, vector<Vec4i> lines, Scalar line_color, int line_width);
void draw(Mat original_img, Vec4i pt, Scalar line_color, int line_width);
Mat drawLanes(Vec2i origin, vector<Vec4i> lines, int cols, Mat original_img);
bool isPointOnLine(Vec2i point, Vec4i line);
void combine(vector<Vec4i> lines, vector<Vec4i> right_lane, vector<Vec4i> left_lane, Mat original_img);
bool close(Vec4i line1, Vec4i line2);
Vec2i findMaxPoint(vector<Vec4i> lines);
Vec2i findMinPoint(vector<Vec4i> lines);
Vec4i averageLine(vector<Vec4i> lines);
double slope(Vec4i line);
int distanceToLine(Vec2i pt1, Vec2i pt2);
double magnitude(double x, double y);

#define BLUE 255

bool close(Vec4i line1, Vec4i line2) {

	double s1 = slope(line1);
	double s2 = slope(line2);

	if (abs(s1 - s2) <= abs(.1*s1)) {//slopes are withing 10% of each other
		if (distanceToLine(Vec2i(line1[0], line1[1]), Vec2i(line2[0], line2[1])) > 10)//bottom to bottom
			return false;
		if (distanceToLine(Vec2i(line1[0], line1[1]), Vec2i(line2[2], line2[3])) > 10)//bottom to top
			return false;
		if (distanceToLine(Vec2i(line1[2], line1[3]), Vec2i(line2[0], line2[1])) > 10)//top to bottom
			return false;
		if (distanceToLine(Vec2i(line1[2], line1[3]), Vec2i(line2[2], line2[3])) > 10)//top to top
			return false;
		return true;
	}
	
	return false;
}

Vec2i findMaxPoint(vector<Vec4i> lines) {

	Vec2i max_point(lines[0][0], 0);

	for (Vec4i line : lines) {
		if (line[1] > max_point[1])
			max_point[1] = line[1];
		else if (line[3] > max_point[1])
			max_point[1] = line[3];
		else if (line[0] > max_point[0])
			max_point[0] = line[0];
		else if (line[2] > max_point[0])
			max_point[0] = line[2];
	}
	
	return max_point;
}

Vec2i findMinPoint(vector<Vec4i> lines) {

	Vec2i min_point(lines[0][0], 0);

	for (Vec4i line : lines) {
		if (line[1] < min_point[1])
			min_point[1] = line[1];
		else if(line[3] < min_point[1])
			min_point[1] = line[3];
		else if (line[0] < min_point[0])
			min_point[0] = line[0];
		else if (line[2] < min_point[0])
			min_point[0] = line[2];
	}
	
	return min_point;
}

Vec4i averageLine(vector<Vec4i> lines) {

	int x1 = 0;
	int x2 = 0;
	int y1 = 0;
	int y2 = 0;

	for (Vec4i line : lines) {
		x1 += line[0];
		x2 += line[2];
		y1 += line[1];
		y2 += line[3];
	}
	
	return Vec4i(x1 / (int)lines.size(), y1 / (int)lines.size(), x2 / (int)lines.size(), y2 / (int)lines.size());
}

int distanceToLine(Vec2i pt1, Vec2i pt2) {
	return (int)sqrt(pow((pt1[0] - pt2[0]), 2) + pow((pt1[1] - pt2[1]), 2));
}

double slope(Vec4i line) {
	return ((double)line[3] - (double)line[1]) / ((double)line[2] - (double)line[0]);
}

double magnitude(double x, double y) {
	return sqrt(x*x + y*y);
}

double perpendicularDistance(Vec4i line, Vec2i pt) {

	Vec2i diff1;
	Vec2i diff2;
	double distance;
	double cross_product;
	double s = slope(line);
	diff1[0] = line[2] - line[0];
	diff1[1] = line[3] - line[1];
	diff2[0] = line[0] - pt[0];
	diff2[1] = line[1] - pt[1];

	cross_product = abs((diff1[0] * diff2[1]) - (diff1[1] * diff2[0]));//cross product of 2d vector is a single value
	distance = cross_product/(magnitude(diff1[0],diff1[1]));
	double temp = distance / sqrt(s*s + 1);
	distance = distance / sqrt(s*s + 1);
	return distance;
	
}

//finds all lines withing 10% of right lane and left lane and combines them.
void combine(vector<Vec4i> lines, vector<Vec4i> right_lane, vector<Vec4i> left_lane, Mat original_img) {

	for (Vec4i line : lines) {
		if (close(right_lane[0], line)) {
			right_lane.push_back(line);
		}
		else if (close(left_lane[0], line)) {
			left_lane.push_back(line);
		}
	}
}

//returns true if the point is on the given line
//given line is held as two points in line
bool isPointOnLine(Vec2i point, Vec4i line) {

	//double slope = 0;
//	double s = 0.0;
	//vertical line
	if (line[0] - line[2] == 0)
		if (point[0] == line[0])//the x values match, the point is on the line
			return true;
		else
			return false;

	double s = slope(line);
	//slope = ((double)line[3] - (double)line[1]) / ((double)line[2] -  (double)line[0]);
	double y_intercept = (double)line[1] - (s*line[0]);

	if ((double)point[1] == (s*point[0]) + y_intercept)
		return true;
	else
		return false;
}

Mat drawLanes(Vec2i origin, vector<Vec4i> lines, int cols, Mat original_img) {

	//choose an approximate pixel value to start looking from(this should be about the center of the right lane where the camera will sit)
	//from here go left or right until you've located a line. Once you find a line go look it up in the list of lines.
	//once you have it look a little further, if you find another very similar line, then that is your lane. draw line on that particular line
	//or add it to a vector called lanes that you then pass to the draw line function
	//repeat this process for the other lane.
	//realistically this function will return a vector<Vec4i> with two lines in it that represent the lanes. and pass this vector to the drawLine function
	
	//line(original_img, Point(2481, 1654), Point(2581, 1754), Scalar(0, 255, 0), 3);
	//this is close to the center of the right lane.
	//initially this algorithm will be centered around this point so that the test image will work
	//this point will change once we start to work with the camera
	//Point(2481,1654)

	double closest_right_distance = cols;
	double closest_left_distance = cols;
	double top_dist, bottom_dist, avg_dist;
	double perp_dist;
	vector<Vec4i> right_lane, left_lane;
	double s;

	for (Vec4i line : lines) {
		s = slope(line);
		if ((abs(s) - .5) <= 0)//cut out lines that are near horizontal
			continue;
		perp_dist = perpendicularDistance(line, origin);
		bottom_dist = distanceToLine(origin, Vec2i(line[0], line[1]));
		top_dist = distanceToLine(origin, Vec2i(line[2], line[3]));
		avg_dist = (bottom_dist + top_dist + perp_dist) / 3;

		if (origin[0] > line[0] || origin[0] > line[2] && s < 0) {//the line is to the left of the origin
			if (perp_dist < closest_left_distance) {
				closest_left_distance = perp_dist;
				if (left_lane.size() == 0)
					left_lane.push_back(line);
				else
					left_lane[0] = line;
			}
		}
		if (origin[0] < line[0] || origin[0] < line[2] && s > 0) {//the line is to the right of the origin
			if (perp_dist < closest_right_distance) {
				closest_right_distance = perp_dist;
				if (right_lane.size() == 0)
					right_lane.push_back(line);
				else
					right_lane[0] = line;
			}
		}
	}
	
	draw(original_img, right_lane, Scalar(0, 255, 0), 5);
	draw(original_img, left_lane, Scalar(0, 0, 255), 5);
	return original_img;
}

//line_color in BGR 
//line_width in pixels
void draw(Mat original_img, vector<Vec4i> lines, Scalar line_color, int line_width) {

	if (lines.size() > 0) {
		for (Vec4i pt : lines) {
			line(original_img, Point(pt[0], pt[1]), Point(pt[2], pt[3]), line_color , line_width);
		}
	}
}

void draw(Mat original_img, Vec4i pt, Scalar line_color, int line_width) {
	line(original_img, Point(pt[0], pt[1]), Point(pt[2], pt[3]), line_color, line_width);
}

Mat ROI(Mat processed_img, vector<vector<Point>> vertices) {//vertices is shape of roi
	
	Mat mask = Mat::zeros(processed_img.size(), processed_img.type());//an empty mask of the size of the image
	int number_of_pts = (int)vertices.size();
	fillPoly(mask, vertices, Scalar(255));//mutates mask,(draws right on the img)
	Mat masked;
	bitwise_and(processed_img, mask,masked);//bitwises the empty mask against the correctly shaped matrix, storing result in mask
	return masked;
}

Mat processImage(Mat original_img) {

	Mat processed_image;
	cvtColor(original_img,processed_image, CV_RGB2GRAY);//since we are gray scaling RGB or BGR doesn't matter

	Canny(processed_image, processed_image, 50, 150);//edge detector

	GaussianBlur(processed_image, processed_image, Size(5,5), 0, 0);//blurs the image

	vector<Point> pts;
	vector<vector<Point>> all_pts;
	pts.push_back(Point(0, 500));
	pts.push_back(Point(0, 720));
	pts.push_back(Point(1280, 720));//these pts represent polygonal shape
	pts.push_back(Point(1280, 500));
	all_pts.push_back(pts);
	processed_image = ROI(processed_image, all_pts);//draw the ROI on the image

	vector<Vec4i> lines;
	HoughLinesP(processed_image, lines, 1, CV_PI / 180, 180, original_img.rows*.3, original_img.rows*.05);//line detector
	processed_image = drawLanes(Vec2i(600, 600), lines, original_img.cols, original_img);
	return processed_image;
}


double clockToMilliseconds(clock_t ticks) {
	// units/(units/time) => time (seconds) * 1000 = milliseconds
	return (ticks / (double)CLOCKS_PER_SEC)*1000.0;
}

int main()
{

	VideoCapture cap("test_straightline_video_720p.mp4");

	if (!cap.isOpened()) {
		cout << "failed to open video";
		return 0;
	}

	Mat frame;
	Mat processed_img;
	cap >> frame;

	for (int i = 0; i < 100; i++)
		cap >> frame;

	clock_t deltaTime = 0;
	unsigned int frames = 0;
	double  frameRate = 30;
	double  averageFrameTimeMilliseconds = 33.333;
	
	int counter = 1;
	while (!frame.empty()) {
		clock_t beginFrame = clock();
		if (counter % 2 == 0) 
			processed_img = processImage(frame);
		else
		{
			counter++;
			cap >> frame;
			continue;
		}
		clock_t endFrame = clock();
		deltaTime += endFrame - beginFrame;
		frames++;

		if (clockToMilliseconds(deltaTime) > 1000.0) { //every second
			frameRate = (double)frames*0.5 + frameRate*0.5; //more stable
			frames = 0;
			deltaTime -= CLOCKS_PER_SEC;
			averageFrameTimeMilliseconds = 1000.0 / (frameRate == 0 ? 0.001 : frameRate);
			cout << "time for a single frame " << averageFrameTimeMilliseconds << endl;
			counter = 0;

		}
			namedWindow("frame", WINDOW_NORMAL);
			imshow("frame", processed_img);
			waitKey(1);
			cout << "frame count: " << counter << endl;
			cap >> frame;
			counter++;
	}
	
	return 0;
}

